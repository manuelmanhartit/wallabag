# Wallabag 1.0

This runs a Wallabag container - I use it for bookmarking all my weblinks. It has a Firefox and a Chrome extension for better access which I still have to test.

You have to create an `.env` file for the configuration of the container to fit your local machine. Please make sure that you backup the syncthing configuration yourself somehow.

If you have more generic questions please have a look [here][7].

## Getting Started

Copy the `.env-example` file into a `.env` file and edit the variables to fit your environment. That means, if you dont share the same thing I do (which is very likely), you have to remove / add the variables in your `.env` file.

    # cp .env-example .env
    # vi .env

Your Wallabag service needs an URL which is set through the `VIRTUAL_HOST` env variable.

After you edited your `.env` file, you have to map the volume mounts accordingly.

    # vi docker-compose.yml

Start the container with the command:

    # sudo docker-compose up -d

### Manual steps

On the first time you have to call the database migration script to .



### Build / Extend

-

### Replicating the service

I suggest that you start with [Getting started](#markdown-header-getting-started) like it is a new service.

### Upgrading the service / containers

For upgrading the container, you will need to call

	# docker-compose down
	
to tear down the service.

Then pull the latest version with

	# docker-compose pull

Lastly start the service with

	# docker-compose up -d

## About the project

### Versioning

We use MAIN.MINOR number for versioning where as MAIN usually means big / breaking changes and MINOR usually means small / non breaking changes. For the version number, see at the top header or in the [tags on this repository][6].

### Author(s)

* **Manuel Manhart** - *Initial work*

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Contributing

If you want to contribute, contact the author.

### Changelog

__1.2__

* Updated readme
* Changed license
* Put into public bitbucket repo

### Open issues

* Uses ROOT as user - which is already defined in the syncthing/syncthing image.
* Uses the host network

## Sources

* [Docker][1]
* [Docker-Compose][2]
* [VS Code][3]
* [Docker Image][4]
* [Running the DB migration][5]
* [Git repository][6]
* [My dockerisation project][7]

[1]: http://www.docker.io/
[2]: https://docs.docker.com/compose/
[3]: https://code.visualstudio.com/
[4]: https://hub.docker.com/r/syncthing/syncthing
[5]: https://github.com/wallabag/docker/issues/154
[6]: https://bitbucket.org/mmprivat/syncthing/downloads/?tab=tags
[7]: https://cms.manhart.space/dockerisierung